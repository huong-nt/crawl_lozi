class DBFunction
  def self.generateXmlFromDB(con, tableName, fileName)
    f = File.new(fileName, "w")
    f.puts "<root>"
    h = getColumnsName(con, tableName)
    res = con.query("SELECT * FROM `#{tableName}` order by name")
    res.each_hash do |row|
      h.each do |column|
        if column == "id"
          id = row[column]
          f.puts "  <place id = \"#{id}\">"
        else
          value = row[column]
          f.puts"   <#{column}><![CDATA[#{value}]]></#{column}>"
        end
      end
      f.puts "  </place>"
    end
    f.puts "</root>"
    f.close
  end

  def self.getColumnsName(con, tableName)
    res = con.query("describe #{tableName}")
    columnsName = Array.new
    res.each do |row|
      columnsName.push(row[0])
    end
    return columnsName
  end

  def self.getStructureOfTable(con, table)
    res = con.query("describe #{table}")
    hash = Hash.new
    res.each do |row|
      hash[row[0]] = row[1]
    end
    hash.delete("id")
    return hash
  end

  def self.deleteTable(con, tablename)
    # puts "Delete table #{tablename}?(y/n)"
    # chon = gets.chomp.downcase
    # if chon=="y"
    # begin
    con.query("DROP TABLE `#{tablename}` ")
  rescue
    # end
    # end
  end

  def self.checkExsitTable(con, tablename)
    res = con.query("show tables")
    res.each { |table|
      if table[0] == tablename
      return true
      end
    }
    return false
  end

  def self.updateTable(con, tablename, field, data_h)
    query = "update `#{tablename}` set `#{field}` = case id "
    data_h.map{|id, value|
      query = query + "when '#{id}' then '#{value}' "
    }
    #    query[query.rindex(",")] = ""
    id_array = data_h.keys.join(", ")
    query = query + " end where `id` in ("+id_array+")"
    #puts query
    con.query(query)
  end

  def self.getRecordNumber(con, table)
    query  = "select count(id) as number from `#{table}`"
    res = con.query(query)
    number = 0
    res.each_hash do |row|
      number = row['number']
      break
    end
    return number
  end

  def self.checkDBIncludeValue(con, table, column, value)
    res = con.query ("select * from `#{table}` where `#{column}` = '#{value}'")
    res.each_hash do |row|
      return true
    end
    return false
  end

  def self.getValueInDB(con, table, column1, column2, value1)
    res = con.query ("select `#{column2}` from `#{table}` where `#{column1}` = '#{value1}'")
    res.each_hash do |row|
      return row[column2].force_encoding("UTF-8")
    end
    return false
  end
end