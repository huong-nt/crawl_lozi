# encoding: UTF-8
require_relative "../initialize.rb"
require_relative ROOT_DIRECTORY+"config/DBConfig.rb"
require 'httparty'
require 'json'

class CrawlLozi
  def initialize(_citynames)
    @city = _citynames
    # @baseLink = _link
    @con = Connector.makeConnect(DBConfig::HOSTNAME, DBConfig::DB_USER, DBConfig::DB_PASS, DBConfig::DB_NAME)
    @source = "lozi"
    @linkTable = "#{@city}_#{@source}_link"
    @detailTable = "#{@city}_#{@source}_detail"
  end

  def createTable
    hash = {
      "link" => "varchar(500)",
      "state" => "varchar(10)",
    }

    Crawl.createTableByHash(@con, @linkTable, hash)

    hash = {
      "name"           => "varchar(500)",
      "address"        => "varchar(500)", 
      "phone"          => "varchar(200)",
      "area"           => "varchar(100)",
      "near"           => "varchar(100)",
      "derection"      => "text",
      "pricerange"     => "varchar(100)",
      "operatingTime"  => "varchar(100)",
      "diningTime"     => "varchar(100)",
      "facilities"     => "varchar(300)",
      "category"       => "varchar(200)",
      "purposes"       => "varchar(400)",
      "description"    => "text",
      "cuisines"       => "varchar(100)",
      "capacity"       => "varchar(50)",
      "longtitude"     => "varchar(50)",
      "latitude"       => "varchar(50)",
      "images"         => "text",
      "link"           => "varchar(300)"
    }
    Crawl.createTableByHash(@con, @detailTable, hash)
  end

  def getLink
    # Empty table link
    @con.query ("truncate table `#{@linkTable}`")
    @con.query ("truncate table `#{@detailTable}`")
    # Get first link from database
    res_arg = @con.query ("select * from `#{@source}_arguments` where `province` = '#{@city}' ")
    res_arg.each_hash do |arg_row|
      link = arg_row['argument']
      index = 0
      while 1
        begin
        case index
          when 0
            response = HTTParty.get(link)
            index = index + 10
            #puts "------case +10"
          when 10..19
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 9
            #puts "------case +9"
          when 28..36
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 8
            #puts "------case +8"
          when 44..65
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 7
            #puts "------case +7"
          when 72..84
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 6
            #puts "------case +6"
          when 90..100
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 5
            #puts "------case +5"
          when 105..113
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 4
            #puts "------case +4"
          when 117..132
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 3
            #puts "------case +3"
          when 135..151
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 2
            #puts "------case +2"
          else
            link_temp = link + "&skip=" + index.to_s + "&limit=10"
            response = HTTParty.get(link_temp)
            index = index + 1
            #puts "------case +1"
          end
          #puts link_temp
          json_temp = JSON.parse(response.body)
          begin
            if(index == 10)
              i = 0
              while i < 10 do
                hash = Hash.new
                begin
                  hash['name'] = json_temp["eateries"][i]["name"]
                rescue Exception => e
                  next
                end
                begin
                  hash['address'] = json_temp["eateries"][i]["fullAddress"]
                rescue Exception => e
                end
                begin
                  hash['phone'] = json_temp["eateries"][i]["phoneNumber"]
                rescue Exception => e
                end
                begin
                  hash['area'] = json_temp["eateries"][i]["area"]
                rescue Exception => e
                end
                begin
                  hash['near'] = json_temp["eateries"][i]["near"]
                rescue Exception => e
                end
                begin
                  hash['derection'] = json_temp["eateries"][i]["direction"]
                rescue Exception => e
                end
                begin
                  hash['pricerange']= "0 đ ~ " + json_temp["eateries"][i]["price"]["max"].to_s + ".000 đ"
                rescue Exception => e
                  hash['pricerange'] = "0 đ ~ 0 đ"
                end
                begin
                  hash['operatingTime'] = json_temp["eateries"][i]["operatingTime"][0]["start"].to_s + " - " + json_temp["eateries"][i]["operatingTime"][0]["finish"].to_s
                rescue Exception => e
                end
                begin
                  hash['diningTime'] = json_temp["eateries"][i]["diningTime"]
                rescue Exception => e 
                end
                begin
                  hash['facilities'] = json_temp["eateries"][i]["facilities"]
                rescue Exception => e
                end
                begin
                  hash['category'] = json_temp["eateries"][i]["category"]
                rescue Exception => e
                end
                perposes = ""
                j = 0
                begin
                  while j < 10 do
                    perpose = json_temp["eateries"][i]["purposes"][j]
                    perposes = perposes + perpose + ", "
                    j += 1
                  end
                rescue Exception => e
                end
                hash['purposes'] = perposes.to_s
                begin
                  hash['description'] = json_temp["eateries"][i]["pageDescription"]
                rescue Exception => e
                end
                begin
                  hash['cuisines'] = json_temp["eateries"][i]["cuisines"][0]
                rescue Exception => e
                end
                begin
                  hash['capacity'] = json_temp["eateries"][i]["capacity"]
                rescue Exception => e
                end
                begin
                  hash['latitude'] = json_temp["eateries"][i]["location"][1]
                rescue Exception => e
                end
                begin
                  hash['longtitude'] = json_temp["eateries"][i]["location"][0]
                rescue Exception => e
                end
                
                image = ""
                begin
                  j = 0
                  while j < 4 do
                    str = json_temp["eateries"][i]["photos"][j]["image"]
                    str = str.split("-")[0]
                    str = str + "-o-720.jpg"
                    image = image + str + " | " 
                    j += 1
                  end 
                rescue Exception => e
                  if j == 0
                    begin
                      str = json_temp["eateries"][i]["avatar"]["xs"]
                      str = str.split("-")[0]
                      str = str + "-o-720.jpg"
                      image = image + str + " | "
                    rescue Exception => e
                    end
                  end
                end
                hash['images'] = image.to_s

                puts hash['link'] = "http://lozi.vn/nha-hang/" + json_temp["eateries"][i]["slug"]

                Crawl.genDataInsert(@con, hash, @detailTable)
                i += 1
                puts "---------------"
              end
            else
              #puts json_temp[0]["name"]
              i = 0
              while i < 10 do
                hash = Hash.new
                begin
                  hash['name'] = json_temp[i]["name"]
                rescue Exception => e
                  next
                end
                begin
                  hash['address'] = json_temp[i]["fullAddress"]
                rescue Exception => e
                end
                begin
                  hash['phone'] = json_temp[i]["phoneNumber"]
                rescue Exception => e
                end
                begin
                  hash['area'] = json_temp[i]["area"]
                rescue Exception => e
                end
                begin
                  hash['near'] = json_temp[i]["near"]
                rescue Exception => e
                end
                begin
                  hash['derection'] = json_temp[i]["direction"]
                rescue Exception => e
                end
                begin
                  hash['pricerange']= "0 đ ~ " + json_temp[i]["price"]["max"].to_s + ".000 đ"
                rescue Exception => e
                  hash['pricerange'] = "0 đ ~ 0 đ"
                end

                begin
                  hash['operatingTime'] = json_temp[i]["operatingTime"][0]["start"].to_s + " - " + json_temp[i]["operatingTime"][0]["finish"].to_s
                rescue Exception => e
                end

                begin
                  hash['diningTime'] = json_temp[i]["diningTime"]
                rescue Exception => e 
                end
                begin
                  hash['facilities'] = json_temp[i]["facilities"]
                rescue Exception => e
                end
                begin
                  hash['facilities'] = json_temp[i]["facilities"]
                rescue Exception => e
                end
                begin
                  hash['category'] = json_temp[i]["category"]
                rescue Exception => e
                end
                perposes = ""
                j = 0
                begin
                  while j < 10 do
                    perpose = json_temp[i]["purposes"][j]
                    perposes = perposes + perpose + ", "
                    j += 1
                  end
                rescue Exception => e
                end
                hash['purposes'] = perposes.to_s
                begin
                  hash['description'] = json_temp[i]["pageDescription"]
                rescue Exception => e
                end
                begin
                  hash['cuisines'] = json_temp[i]["cuisines"][0]
                rescue Exception => e
                end
                begin
                  hash['capacity'] = json_temp[i]["capacity"]
                rescue Exception => e
                end
                begin
                  hash['latitude'] = json_temp[i]["location"][1]
                rescue Exception => e
                end
                begin
                  hash['longtitude'] = json_temp[i]["location"][0]
                rescue Exception => e
                end
                
                image = ""
                begin
                  j = 0
                  while j < 4 do
                    str = json_temp[i]["photos"][j]["image"]
                    str = str.split("-")[0]
                    str = str + "-o-720.jpg"
                    image = image + str + " | " 
                    j += 1
                  end 
                rescue Exception => e
                  if j == 0
                    begin
                      str = json_temp[i]["avatar"]["xs"]
                      str = str.split("-")[0]
                      str = str + "-o-720.jpg"
                      image = image + str + " | "
                    rescue Exception => e
                    end
                  end
                end
                hash['images'] = image.to_s

                puts hash['link'] = "http://lozi.vn/nha-hang/" + json_temp[i]["slug"]

                Crawl.genDataInsert(@con, hash, @detailTable)
                i += 1
                puts "---------------"
              end
            end
              
          rescue Exception => e
            puts e
            puts "ERROR 2"
          end

          
        rescue Exception => e
          puts "ERROR PARSING!"
          puts e
          break 
        end
      end
      puts "Finish get LOZZI.VN!"

    end
  end
  end